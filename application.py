import requests
import pandas as pd

# une fonction qui télécharge des trucs
def get_dataset():
    with open("./dataset.txt", "a") as f:
        result = requests.get("https://nskm.xyz/assets/dataset.txt").content
        f.write(result.decode())


# une fonction qui supprime la moitié des lignesb
def drop_lines():
    df = pd.read_csv("./dataset.txt")
    mid = len(df.index) // 2
    df.drop(df.index[mid:], inplace=True)
    df.to_csv("./dataset_reduced.csv")
